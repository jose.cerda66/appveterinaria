-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-07-2019 a las 17:57:12
-- Versión del servidor: 10.3.16-MariaDB
-- Versión de PHP: 7.1.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `veterinaria`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id_cliente` int(10) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `rut` int(15) NOT NULL,
  `direccion` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `fono` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id_cliente`, `nombre`, `rut`, `direccion`, `email`, `fono`) VALUES
(1, 'José', 171077907, 'pasaje chañar 610', 'jose.cerda66@gmail.com', 953513634),
(2, 'Karen', 167789987, 'calle falsa 123', 'karen@gmail.com', 912345678),
(3, 'Manuel', 235564778, 'ocarrol 255', 'manuel@gmail.com', 534352617);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mascota`
--

CREATE TABLE `mascota` (
  `id_mascota` int(11) NOT NULL,
  `nombre_mascota` varchar(50) NOT NULL,
  `tipo_mascota` varchar(50) NOT NULL,
  `raza` varchar(50) NOT NULL,
  `sexo` varchar(50) NOT NULL,
  `vacunas` varchar(50) NOT NULL,
  `color` varchar(50) NOT NULL,
  `edad` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `mascota`
--

INSERT INTO `mascota` (`id_mascota`, `nombre_mascota`, `tipo_mascota`, `raza`, `sexo`, `vacunas`, `color`, `edad`) VALUES
(1, 'Boby', 'Perro', 'golden retriever', 'Macho', 'Si', 'Marrón', 9),
(2, 'Elfa', 'Ave', 'ninfa', 'Hembra', 'No', 'Amarillo', 1),
(3, 'Javier', 'Ave', 'canario', 'Macho', 'Si', 'Amarillo', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio`
--

CREATE TABLE `servicio` (
  `id_cliente` int(11) NOT NULL,
  `nombre_cliente` varchar(50) NOT NULL,
  `nombre_mascota` varchar(50) NOT NULL,
  `detalle_revision` varchar(100) NOT NULL,
  `servicio_` varchar(100) NOT NULL,
  `vacunas` varchar(11) NOT NULL,
  `fecha_vacunas` date NOT NULL,
  `total` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `servicio`
--

INSERT INTO `servicio` (`id_cliente`, `nombre_cliente`, `nombre_mascota`, `detalle_revision`, `servicio_`, `vacunas`, `fecha_vacunas`, `total`) VALUES
(1, 'José', 'Boby', 'asdf', 'Cirugía', 'Si', '2019-07-02', 12000),
(6, 'Karen', 'Elfa', 'asdasda', 'Vacunación', 'Si', '2019-07-09', 35000),
(7, 'Manuel', 'Javier', 'Revisión General', 'Limpieza de oídos', 'Si', '2019-07-11', 29990);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id_cliente`);

--
-- Indices de la tabla `mascota`
--
ALTER TABLE `mascota`
  ADD PRIMARY KEY (`id_mascota`);

--
-- Indices de la tabla `servicio`
--
ALTER TABLE `servicio`
  ADD PRIMARY KEY (`id_cliente`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id_cliente` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `mascota`
--
ALTER TABLE `mascota`
  MODIFY `id_mascota` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `servicio`
--
ALTER TABLE `servicio`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
