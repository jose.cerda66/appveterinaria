﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using MySql.Data.MySqlClient;

namespace ENA
{
    public partial class IngresarCliente : Form
    {
         string sexo;
         string vacunas;

        public IngresarCliente()
        {

            InitializeComponent();
           
           
            
        }

        
        private void button1_Click(object sender, EventArgs e)
        {

            if (radioButton1.Checked == true)
            {
                sexo = "Macho";
            }
            if (radioButton2.Checked == true)
            {
                sexo = "Hembra";
            }
            if (radioButton3.Checked == true)
            {
                vacunas = "Si";
            }
            if (radioButton2.Checked == true)
            {
                vacunas = "No";

            }

            string server = "localhost";
            string puerto = "3306";
            string user = "root";
            string pwd = "";
            string database = "veterinaria";
            //INSERT INTO cliente(`nombre`, `rut`, `direccion`, `email`, `fono`) VALUES ('" + textBox1.Text + "', '" + textBox2.Text + "', '" + textBox3.Text + "', '" + textBox4.Text + "', '" + textBox5.Text + "')
            string consulta = "INSERT INTO cliente(`nombre`, `rut`, `direccion`, `email`, `fono`) VALUES ('" + textBox1.Text + "', '" + textBox2.Text + "', '" + textBox3.Text + "', '" + textBox4.Text + "', '" + textBox5.Text + "')";
            //INSERT INTO mascota() VALUES ()

            string consulta2 = "INSERT INTO mascota(`nombre_mascota`, `tipo_mascota`, `raza`, `sexo`, `vacunas`, `color`, `edad`) VALUES ('" + textBox9.Text + "', '" + comboBox1.Text + "', '" + comboBox2.Text + "', '" + sexo + "', '" + vacunas + "', '" + textBox8.Text + "', '" + textBox10.Text + "')";

            string connStr = "server=" + server + ";uid=" + user + ";pwd=" + pwd + ";database=" + database + ";port=" + puerto;
            try
            {

             
               //Aquí se crea un obtejo conexión que luego se abre a través del metodo open
                MySqlConnection basededatos = new MySqlConnection(connStr);
                basededatos.Open();
               
                //Aquí se declara un objeto de tipo commando mysql
                //al cual se le pasa la cadena con la conexión y la consulta
                MySqlCommand ejecutarlaconsulta = new MySqlCommand(consulta, basededatos);
                MySqlCommand ejecutarconsulta2 = new MySqlCommand(consulta2, basededatos);
                MySqlDataReader lector;
                MySqlDataReader lector2;
                int i = 0;
                // Ejecuta la consultas
                lector = ejecutarlaconsulta.ExecuteReader();
                
                    
                         
                lector.Close();
                lector2 = ejecutarconsulta2.ExecuteReader();
               
                    
                   
                
                
                lector2.Close();

                basededatos.Close();
                MessageBox.Show("Los datos han sido registrados exitosamente");

            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error al conectar al servidor de MySQL: " +
                    ex.Message, "Error al conectar",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);


            }
           


           
            

        }

        private void IngresarCliente_Load(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            

                if (comboBox1.Text == "Perro")
                {
                    comboBox2.Items.Clear();
                    comboBox2.Items.Add("labrador");
                    comboBox2.Items.Add("doberman");
                    comboBox2.Items.Add("golden retriever");
                }
                if (comboBox1.Text == "Gato")
                {
                    comboBox2.Items.Clear();
                    comboBox2.Items.Add("siames");
                    comboBox2.Items.Add("persa");
                    comboBox2.Items.Add("angora");
                }
                if (comboBox1.Text == "Ave")
                {
                    comboBox2.Items.Clear();
                    comboBox2.Items.Add("canario");
                    comboBox2.Items.Add("cotorra");
                    comboBox2.Items.Add("ninfa");
                }
            
            
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
            IngresarControlServicio ventana = new IngresarControlServicio();
            ventana.Show();
            
             
        }
    }
}
