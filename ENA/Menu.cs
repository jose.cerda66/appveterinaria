﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ENA
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
            IngresarCliente objeto1 = new IngresarCliente();
            objeto1.Show();
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            IngresarControlServicio objeto2 = new IngresarControlServicio();
            objeto2.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ConsultarControlServicio objeto3 = new ConsultarControlServicio();
            objeto3.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            EliminarControlServicio ventana = new EliminarControlServicio();
            ventana.Show();
        }
    }
}
