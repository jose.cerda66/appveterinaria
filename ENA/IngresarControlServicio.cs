﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using MySql.Data.MySqlClient;

namespace ENA
{
    public partial class IngresarControlServicio : Form
    {
        string dato5;
        string server = "localhost";
        string puerto = "3306";
        string user = "root";
        string pwd = "";
        string database = "veterinaria";
        string consulta = "SELECT nombre FROM cliente";
        string consulta2 = "SELECT nombre_mascota FROM mascota;";
        public IngresarControlServicio()
        {
            InitializeComponent();

            

            string connStr = "server=" + server + ";uid=" + user + ";pwd=" + pwd + ";database=" + database + ";port=" + puerto;
            try
            {
                MySqlConnection conexion = new MySqlConnection(connStr);
                conexion.Open();
                MySqlDataReader registrosObtenidosMySQL = null;
                MySqlDataReader registrosObtenidos2MySQL = null;

                MySqlCommand cmd = new MySqlCommand(consulta, conexion);
                MySqlCommand cmd2 = new MySqlCommand(consulta2, conexion);

                registrosObtenidosMySQL = cmd.ExecuteReader();

                comboBox1.Items.Clear();
                comboBox2.Items.Clear();
                while (registrosObtenidosMySQL.Read())
                {
                    comboBox1.Items.Add(registrosObtenidosMySQL.GetString(0));
                }
                registrosObtenidosMySQL.Close();

                registrosObtenidos2MySQL = cmd2.ExecuteReader();
                while (registrosObtenidos2MySQL.Read())
                {
                    comboBox2.Items.Add(registrosObtenidos2MySQL.GetString(0));
                }
                registrosObtenidos2MySQL.Close();
                conexion.Close();
            }
            catch(MySqlException ex){

                MessageBox.Show("Error al conectar: " +ex);

            }

         
            
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            
        }

        private void IngresarControlServicio_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string dato1 = comboBox1.SelectedItem.ToString();
            string dato2 = comboBox2.SelectedItem.ToString();
            string dato3 = richTextBox1.Text;
            string dato4 = comboBox3.SelectedItem.ToString();
            
            if (radioButton1.Checked == true  ) {
                dato5 = "Si";
                
            }
            if (radioButton2.Checked == true)
            {
                dato5 = "No";
               
            }

            DateTime dateTimeVariable = dateTimePicker1.Value;
            string date = dateTimeVariable.ToString("yyyy-MM-dd");
            

            

            /*INSERT INTO `servicio` (`id_servicio`, `nombre_cliente`, `nombre_mascota`, `detalle_revision`, `servicio_`, `vacunas`, `fecha_vacunas`, `total`) VALUES (NULL, 'Karen', 'Lazzy', 'as', 'sa', 'Si', '2019-06-25', '29990');*/

            double dato7 = double.Parse(textBox1.Text);

            

            string cadena = "server=" + server + ";uid=" + user + ";pwd=" + pwd + ";database=" + database + ";port=" + puerto;
            MySqlConnection con = new MySqlConnection(cadena);
            try
            {
                con.Open();
                //"INSERT INTO `servicio` (`id_servicio`, `nombre_cliente`, `nombre_mascota`, `detalle_revision`, `servicio_`, `vacunas`, `fecha_vacunas`, `total`) VALUES (NULL, 'José', 'lazy', 'asdasd', 'asd', 'Si', '2019-06-26', '15.000');"
                String insert = "INSERT INTO `servicio` (nombre_cliente, nombre_mascota, detalle_revision, servicio_, vacunas, fecha_vacunas, total) VALUES ('" + dato1 + "', '" + dato2 + "', '" + dato3 + "', '" + dato4 + "', '" + dato5 + "','" + date + "', '" + dato7 + "');";
                MySqlCommand comando = new MySqlCommand(insert, con);
                comando.ExecuteNonQuery();
                MessageBox.Show("Datos ingresados correctamente "+date);
            }
            catch(MySqlException ex){
                MessageBox.Show("Error al insertar los datos "+ex);
            }
            
            


        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
            ConsultarControlServicio ventana = new ConsultarControlServicio();
            ventana.Show();
        }
    }
}
