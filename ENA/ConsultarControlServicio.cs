﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace ENA
{

    public partial class ConsultarControlServicio : Form
    {
        
        public ConsultarControlServicio()
        {
            InitializeComponent();
            MySqlDataReader reader = null;
            try
            {
                
                string server = "localhost";
                string puerto = "3306";
                string user = "root";
                string pwd = "";
                string database = "veterinaria";
                string consulta = "SELECT * FROM servicio";
                string consulta2 = "select * from servicio";

                string con = "server=" + server + ";uid=" + user + ";pwd=" + pwd +";database="+database+  ";port=" + puerto;
                MySqlConnection conexion = new MySqlConnection(con);
                conexion.Open();
                
 
                

                
                MySqlCommand comando = new MySqlCommand(consulta2, conexion);
                MySqlDataReader lector = comando.ExecuteReader();
                
                while (lector.Read())
                {
                    comboBox1.Items.Add(lector.GetString("nombre_cliente"));
                    comboBox2.Items.Add(lector.GetString("nombre_mascota"));
                }
                lector.Close();


                MySqlDataAdapter adaptador = new MySqlDataAdapter();
                adaptador.SelectCommand = new MySqlCommand(consulta, conexion);
                DataTable tabla = new DataTable();
                adaptador.Fill(tabla);

                /*Bindear los datos con la tabla y el datasource la fuente de datos a la tabla*/
                BindingSource bSource = new BindingSource();
                bSource.DataSource = tabla;
                /*el datagriview1 mas el datasource es igual al binding source*/
                dataGridView1.DataSource = bSource;


                
               /*
                while (reader.Read())
                {
                    dataGridView1.Columns.Add("titulo","123");
                    dataGridView1.Rows.Add(reader.ToString());

                }
                * */
                conexion.Close();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error al obtener la lista de tablas " +
                    "de la BD de MySQL: " +
                    ex.Message, "Error obtener tablas",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
            }
        }

        private void ConsultarControlServicio_Load(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string server = "localhost";
            string puerto = "3306";
            string user = "root";
            string pwd = "";
            string database = "veterinaria";
            string cliente = comboBox1.Text;
            string mascota = comboBox2.Text;

            string con = "server=" + server + ";uid=" + user + ";pwd=" + pwd + ";database=" + database + ";port=" + puerto;
            MySqlConnection conexion = new MySqlConnection(con);
            conexion.Open();

            
            

            if (comboBox1.SelectedItem != null ) {

                string buscar = "SELECT * from cliente WHERE nombre= '" + cliente + "'";
                MySqlDataAdapter adaptador = new MySqlDataAdapter();
            adaptador.SelectCommand = new MySqlCommand(buscar, conexion);
            DataTable tabla = new DataTable();
            adaptador.Fill(tabla);
            /*Bindear los datos con la tabla y el datasource la fuente de datos a la tabla*/
            BindingSource bSource = new BindingSource();
            bSource.DataSource = tabla;
            /*el datagriview1 mas el datasource es igual al binding source*/
            dataGridView1.DataSource = bSource;

                comboBox1.SelectedItem = null; 
            }
            else if (comboBox2.Text != null)
            {
                
                
                string buscar = "SELECT * from mascota WHERE nombre_mascota= '" + mascota + "'";
                MySqlDataAdapter adaptador = new MySqlDataAdapter();
                adaptador.SelectCommand = new MySqlCommand(buscar, conexion);
                DataTable tabla = new DataTable();
                adaptador.Fill(tabla);
                /*Bindear los datos con la tabla y el datasource la fuente de datos a la tabla*/
                BindingSource bSource = new BindingSource();
                bSource.DataSource = tabla;
                /*el datagriview1 mas el datasource es igual al binding source*/
                dataGridView1.DataSource = bSource;
                comboBox2.SelectedItem = null;
            }

            if((comboBox1.Text == null) && (comboBox2.Text == null))
            {

                string buscar = "SELECT * from servicio";
                MySqlDataAdapter adaptador = new MySqlDataAdapter();
                adaptador.SelectCommand = new MySqlCommand(buscar, conexion);
                DataTable tabla = new DataTable();
                adaptador.Fill(tabla);
                /*Bindear los datos con la tabla y el datasource la fuente de datos a la tabla*/
                BindingSource bSource = new BindingSource();
                bSource.DataSource = tabla;
                /*el datagriview1 mas el datasource es igual al binding source*/
                dataGridView1.DataSource = bSource;
                

            }
    }
}
}
